<?php

include 'Parsedown.php';
include 'config.php';

// insert RSS header stuff
header('Content-Type: application/xml');
echo '<?xml version="1.0" encoding="UTF-8"?>';
echo '<rss version="2.0" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:wfw="http://wellformedweb.org/CommentAPI/" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:sy="http://purl.org/rss/1.0/modules/syndication/" xmlns:slash="http://purl.org/rss/1.0/modules/slash/">';
echo '<channel>';
echo '<title>' . $feed['title'] . '</title>';
echo '<link>' . $feed['link'] . '</link>';
echo '<description>' . $feed['description'] . '</description>';

// get the data
$dir = "./seeds";
$files = scandir($dir, 1);

// ignore some stuff when you are scanning the ./seeds directory
$hideName = array(
    '.',
    '..',
    '.git',
    '.gitignore',
    '.DS_Store',
    'drafts'
);

$Parsedown = new Parsedown();
$i = 1;

// display the data as RSS
foreach($files as $filename) {
	if (!in_array($filename, $hideName)) {

        // read file contents
    	$text = file_get_contents($dir . '/' . $filename);

    	// the most fragile part of this whole kit and kaboodle
    	$raw_date = str_replace('.md', '', $filename);
    	$rss_date = DateTime::createFromFormat(DATE_RFC3339, $raw_date)->format(DATE_RSS);

        // display file contents
        echo '<item>';
	    echo '<title>' . $feed['title'] . '-' . $rss_date . '</title>';
	    echo '<link>' . $feed['link'] . '/' . $i . '</link>';
	    echo '<pubDate>' . $rss_date . '</pubDate>';
	    echo '<guid>' . $feed['link'] . '/' . $raw_date . '</guid>';
	    echo '<content:encoded><![CDATA[' . $Parsedown->text($text) . ']]></content:encoded>';
	    echo '</item>';

	    // limit loop to X times
        if ($i++ == $feed['count']) break;
    }
}

echo '</channel></rss>';

?>