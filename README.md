# weeds

A little pile of tools to host your own weed, an RSS-only weblog.

## `weeds.php`

A very little bit of PHP that'll convert markdown files saved to the `/seeds` directory into an RSS feed.

Files in the `/seeds` directory have to have wakadoo filenames, a la `2021-03-31T21:43:57-0400.md`.

Configuration all controlled by `config.php`.

## `dandelion.sh`

A bash-tastic little script to help you seed your weeds. 

```
dandelion -- cultivate weeds

              `::`
               /
              `    `;:`
        .;:;        /
        ::;        `
      _ ';:;;'
      >'. ||  _
      `> \||.'<
        `>|/ <`
         `||/`
   ^jgs^^^^^^^^^^^

commands:

  help  - display this message
  draft - create a new draft weed in /seeds/drafts
  plant - generate a static xml file of your feed
        - passing no arguments will create a new weed right off
	    	
```

## future?

Probably port this away from relying at all on PHP