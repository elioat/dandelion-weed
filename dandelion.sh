#! /usr/bin/env bash

EDITOR=${EDITOR:-vi}

die(){
    (($#)) && printf >&2 '%s\n' "$@"
    exit 1
}

cd "$(dirname "$0")"

USER_INPUT="$@"

ISO_DATE=$(date +%Y-%m-%dT%H:%M:%S%z)
YEAR=$(date +'%Y')
MONTH=$(date +'%m')
DAY=$(date +'%d')
FILE_NAME=${ISO_DATE}.md


if [ -n "$USER_INPUT" ]; then

	if [ $USER_INPUT == "help" ]; then

		echo -e "\ndandelion -- cultivate weeds\n"
		FLOWER="CQkgICAgICAgICAgYDo6YAogICAgICAgICAgICAgICAvCiAgICAgICAgICAgICAgYCAgICBgOzpgCiAgICAgICAgLjs6OyAgICAgICAgLwogICAgICAgIDo6OyAgICAgICAgYAogICAgICBfICc7Ojs7JwogICAgICA+Jy4gfHwgIF8KICAgICAgYD4gXHx8Lic8CiAgICAgICAgYD58LyA8YAogICAgICAgICBgfHwvYAogICBeamdzXl5eXl5eXl5eXl4="
		echo -e $FLOWER | base64 -d
		echo -e "\n\ncommands:\n"
		echo -e "	help	- display this message"
		echo -e "	draft	- create a new draft weed in /seeds/drafts"
		echo -e "	plant	- generate a static xml file of your feed"
		echo -e "	    	- passing no arguments will create a new weed right off"
		echo -e "\n\n"

	fi

	if [ $USER_INPUT == "draft" ]; then

		THE_PATH=./seeds/drafts

		FILE_AND_PATH=${THE_PATH}/${FILE_NAME}

		mkdir -p ${THE_PATH}
		touch ${FILE_AND_PATH}

		$EDITOR ${FILE_AND_PATH}

	fi

	if [ $USER_INPUT == "plant" ]; then

		php weeds.php > weed.rss

	fi

else

	THE_PATH=./seeds

	FILE_AND_PATH=${THE_PATH}/${FILE_NAME}

	mkdir -p ${THE_PATH}
	touch ${FILE_AND_PATH}

	$EDITOR ${FILE_AND_PATH}

fi
